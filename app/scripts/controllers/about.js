'use strict';

/**
 * @ngdoc function
 * @name demoAngularApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the demoAngularApp
 */
angular.module('demoAngularApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
